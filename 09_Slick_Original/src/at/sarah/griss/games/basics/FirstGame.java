package at.sarah.griss.games.basics;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.tests.AnimationTest;

public class FirstGame extends BasicGame {

	private int statusR; // right=0; down=1; left=2; up=3
	private int statusC; // right=0 left=1
	private int statusO; // up=0 down=1

	private double xRect;
	private double yRect;
	private double xCircle;
	private double yOval;

	public FirstGame() {
		super("Firts Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {
		// graphics.drawString("Hello",(int) this.x, 100);

		// graphics.drawRect((int) this.xrect, (int) this.yrect, 40, 40);
		// graphics.drawOval((int) this.xcircle, 145, 100, 100);
		// graphics.drawOval(350, (int)this.yoval, 50, 30);
		graphics.setColor(Color.green);
		graphics.fillRect((int) this.xRect, (int) this.yRect, 40, 40);
		graphics.setColor(Color.pink);
		graphics.fillOval((int) this.xCircle, 145, 100, 100);
		graphics.setColor(Color.magenta);
		graphics.fillOval(350, (int) this.yOval, 50, 30);

	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.xCircle = 0;
		this.xRect = 10;
		this.yRect = 50;
		this.yOval = 10;
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		// Circle
		if (statusC == 0) {
			this.xCircle += (double) delta * 0.5;
			if (this.xCircle >= 700) {
				this.statusC = 1;
			}
		}

		if (statusC == 1) {
			this.xCircle += (double) delta * (-1 * 0.5);
			if (this.xCircle <= 10) {
				this.statusC = 0;
			}
		}

		// Rectangle
		if (this.xRect <= 600 && this.yRect <= 50) {
			this.xRect += (double) delta * 0.5;
			statusR = 0;
		} else if (this.xRect >= 600 && this.yRect <= 500) {
			this.yRect += (double) delta * 0.5;
			statusR = 1;
		} else if (this.xRect >= 10 && this.yRect >= 400) {
			this.xRect += (double) delta * (-1 * 0.5);
			statusR = 2;
		} else if (this.xRect <= 10 && this.yRect >= 50) {
			this.yRect += (double) delta * (-1 * 0.5);
			statusR = 3;
		}

		// Oval
		if (statusO == 0) {
			this.yOval += (double) delta * 0.5;
			if (this.yOval >= 600) {
				this.yOval = 10;
			}
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
