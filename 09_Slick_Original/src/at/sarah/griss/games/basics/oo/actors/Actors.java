package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.Graphics;

public interface Actors {
	
	public void update(int delta);
	public void render(Graphics graphics);

}
