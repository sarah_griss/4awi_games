package at.sarah.griss.games.basics.oo.movestrategy;

public class MoveLeftRightStrategy implements MoveStrategy {
	private double x, y;
	private int status = 0;

	public MoveLeftRightStrategy(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getX() {
		return this.x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getY() {
		return this.y;
	}

	@Override
	public void update(int delta) {
		//System.out.println(this.x + " - " + this.y);

		if (this.status == 0) {
			this.x += (double) delta * 0.5;
			if (this.x >= 500) {
				this.status = 1;
			}
		} else if (this.status == 1) {
			this.x += (double) delta * (-1 * 0.5);
			if (this.x <= 10) {
				this.status = 0;
			}
		}

	}

}
