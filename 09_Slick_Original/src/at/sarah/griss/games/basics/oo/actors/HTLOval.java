package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HTLOval implements Actors {
	private double x, y;
	private int width, height;
	private int statusO; // up=0 down=1

	public HTLOval(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {
		if (statusO == 0) {
			this.y += (double) delta * 0.5;
			if (this.y >= 600) {
				this.y = 10;
			}
		}

	}

	public void render(Graphics graphics) {
		// graphics.setColor(Color.cyan);
		graphics.drawOval((int) this.x, (int) this.y, this.width, this.height);
		// graphics.fillOval((int) this.x, (int) this.y, this.width, this.height);
		// graphics.setColor(Color.white);
	}
}
