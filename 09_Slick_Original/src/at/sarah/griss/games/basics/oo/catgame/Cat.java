package at.sarah.griss.games.basics.oo.catgame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class Cat {
	private static float x = 800;
	private static float y = 930;
	private GameContainer container;
	private Image Cat;
	private Shape shape;
	private List<Enemy> enemies;
	private FirstGame_Cat game;

	public Cat(GameContainer container, FirstGame_Cat game) {
		super();
		this.game = game;
		this.container = container;
		this.shape = new Rectangle(x, y, 80, 100);

		try {
			this.Cat = new Image("testdata/cat2.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.enemies = new ArrayList<>();
	}

	public void addEnemey(Enemy enemy) {
		this.enemies.add(enemy);
	}

	public static double getX() {
		return x;
	}

	public static double getY() {
		return y;
	}

	public void update(int delta, GameContainer container) {

		if (container.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x -= delta;
		}

		if (container.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x += delta;
		}

		this.shape.setX(this.x);

		/**
		 * Testing Collission with Enemies
		 */
		for (Enemy enemy : enemies) {

			if (enemy.getShape().intersects(this.shape)) {
				if (enemy instanceof GoodFish) {
					GoodFish goodFish = (GoodFish) enemy.getCollisionObject();
					if (!goodFish.getCollision()) {
						game.increase();
					}
					goodFish.setCollission(true);
				}

				if (enemy instanceof BadFish) {
					BadFish badFish = (BadFish) enemy.getCollisionObject();
					if (enemy.getShape().intersects(this.shape)) {
						if (!badFish.getCollision()) {
							System.out.println("YOU FAILED !");
							game.reset();
						}
						badFish.setCollission(true);
					}
				}

//			if (!enemy.getShape().intersects(this.shape) && goodFish.getCollision() == true) {
//				goodFish.setCollission(false);
//			}
			}
		}
	}

	public void render(Graphics graphics) {

		this.Cat.draw((int) this.x, (int) this.y);
		graphics.setColor(Color.transparent);
		graphics.draw(this.shape);

	}

}
