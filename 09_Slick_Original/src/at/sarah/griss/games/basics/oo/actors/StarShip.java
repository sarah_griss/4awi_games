package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;

public class StarShip implements Actors {
	private static double x = 500;
	private static double y = 500;
	private GameContainer container;
	
	public StarShip(GameContainer container) {
		this.container = container;
	}

	public static double getX() {
		return x;
	}

	public static double getY() {
		return y;
	}

	@Override
	public void update(int delta) {
		
		if (container.getInput().isKeyDown(Input.KEY_LEFT)) {
			this.x--;
		}
		
		if (container.getInput().isKeyDown(Input.KEY_RIGHT)) {
			this.x++;
		}
		
	}

	@Override
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, 20, 20);

	}

}
