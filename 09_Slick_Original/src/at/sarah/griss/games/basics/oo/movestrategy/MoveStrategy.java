package at.sarah.griss.games.basics.oo.movestrategy;

public interface MoveStrategy {
	public void update(int delta);
	public double getX();
	public double getY();

}
