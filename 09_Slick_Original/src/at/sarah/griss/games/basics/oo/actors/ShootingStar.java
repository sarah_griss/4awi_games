package at.sarah.griss.games.basics.oo.actors;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class ShootingStar implements Actors {
	private float size;
	private double speed;
	private double x, y;

	public ShootingStar( double speed, int size) {
		super();
		this.size = size;
		this.speed = speed;
		
		setRandomPosition();
	}
	
	private void setRandomPosition() {
		Random r = new Random();
		this.y = r.nextInt(400);
		this.x = r.nextInt(100)-5000;
	}
	
	private void setRandomSize()
	{
		Random r2 = new Random();
		this.size = r2.nextInt(16);
	}

	@Override
	public void update(int delta) {

		this.x += (double) delta * 0.5;
		if(this.x > 800)
		{
			setRandomPosition();
			setRandomSize();
		}
		
		if(this.x > 0)
		{
			
		
		for(int i = 0; i < 500; i++)
		{
			this.size = (float) (this.size + 0.001);
		}
		}
	}

	@Override
	public void render(Graphics graphics) {
		
		graphics.setColor(Color.yellow);
		graphics.drawOval((int) this.x, (int) this.y, size, size);
		graphics.fillOval((int) this.x, (int) this.y, size, size);
		graphics.setColor(Color.white);
		
	}

}
