package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Bullet implements Actors {
	private double x, y;
	private Image bulletImage;

	public Bullet(double x, double y) {
		super();
		this.x = x;
		this.y = y;

		try {
			this.bulletImage = new Image("testdata/logo.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void update(int delta) {
		this.y -= (double) delta * 0.8;

	}

	@Override
	public void render(Graphics graphics) {
		graphics.fillOval((int) this.x, (int) this.y, 20, 20);
		this.bulletImage.draw((int) this.x, (int) this.y);
	}

}
