package at.sarah.griss.games.basics.oo.catgame;

import org.newdawn.slick.geom.Shape;

public interface Enemy {

	public Shape getShape();

	public Object getCollisionObject();
}
