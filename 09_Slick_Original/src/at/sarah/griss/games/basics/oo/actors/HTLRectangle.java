package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class HTLRectangle implements Actors {

	private double x, y;
	private int width, height;
	private int statusR; // right=0; down=1; left=2; up=3

	public HTLRectangle(double x, double y, int width, int height) {
		super();
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {
		if (this.x <= 600 && this.y <= 50) {
			this.x += (double) delta * 0.5;
			statusR = 0;
		} else if (this.x >= 600 && this.y <= 500) {
			this.y += (double) delta * 0.5;
			statusR = 1;
		} else if (this.x >= 10 && this.y >= 400) {
			this.x += (double) delta * (-1 * 0.5);
			statusR = 2;
		} else if (this.x <= 10 && this.y >= 50) {
			this.y += (double) delta * (-1 * 0.5);
			statusR = 3;
		}

	}

	public void render(Graphics graphics) {

		// graphics.setColor(Color.magenta);
		graphics.drawRect((int) this.x, (int) this.y, this.width, this.height);
		// graphics.fillRect((int) this.x, (int) this.y, this.width, this.height);
		// graphics.setColor(Color.white);

	}

}
