package at.sarah.griss.games.basics.oo.actors;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import at.sarah.griss.games.basics.oo.movestrategy.MoveStrategy;

public class HTLCircle implements Actors {
	private MoveStrategy mls;
	private double x, y;
	private int width, height;
	private int statusC; // right=0 left=1

	public HTLCircle(int width, int height, MoveStrategy mls) {
		super();
		this.mls = mls;
		this.width = width;
		this.height = height;
	}

	public void update(int delta) {

		this.mls.update(delta);
		

	}

	public void render(Graphics graphics) {
		// graphics.setColor(Color.gray);
		graphics.drawOval((int) this.mls.getX(),(int) this.mls.getY(), this.width,this.height);
		// graphics.fillOval((int) this.x, (int) this.y, this.width, this.height);
		// graphics.setColor(Color.white);

	}

}
