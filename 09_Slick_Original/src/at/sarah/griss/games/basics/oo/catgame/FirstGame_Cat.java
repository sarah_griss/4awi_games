package at.sarah.griss.games.basics.oo.catgame;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.Font;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import at.sarah.griss.games.basics.oo.FirstGame;

public class FirstGame_Cat extends BasicGame {
	private Cat cat;
	private GoodFish goodFish;
	private BadFish badFish;
	private float Counter;
	private boolean isInterrupted = false;
	private int timeSinceInterruption = 0;

	public FirstGame_Cat() {
		super("Cat Game");
	}

	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

		if (!isInterrupted) {
			cat.render(graphics);
			goodFish.render(graphics);
			badFish.render(graphics);
		} else {
			graphics.setColor(Color.black);
			graphics.fillRect(850, 40, 115, 35);
			graphics.drawRect(850, 40, 115, 35);

			graphics.setColor(Color.red);
			graphics.drawString("YOU FAILED!", 860, 50);
		}
		// Countertext
		graphics.setColor(Color.white);
		String scoreText = "Fish: " + (int) Counter;
		graphics.drawString(scoreText, 1800, 20);

		// Backgorund
		graphics.setBackground(Color.lightGray);

	}

	public void init(GameContainer gameContainer) throws SlickException {
		this.cat = new Cat(gameContainer, this);

		for (int i = 0; i < 50; i++) {
			this.goodFish = new GoodFish(gameContainer);
		}

		for (int i = 0; i < 50; i++) {
			this.badFish = new BadFish(gameContainer);
		}

		this.cat.addEnemey(badFish);
		this.cat.addEnemey(goodFish);
	}

	public void update(GameContainer gameContainer, int delta) throws SlickException {
		cat.update(delta, gameContainer);
		goodFish.update(delta);
		badFish.update(delta);

		if (badFish.getCollision() == true) {
			this.isInterrupted = true;
		}

		if (this.isInterrupted) {
			timeSinceInterruption += delta;
			if (timeSinceInterruption > 2000) {
				this.isInterrupted = false;
				this.timeSinceInterruption = 0;
			}
		}
	}

	/**
	 * Counter for GoodFish
	 */
	public void increase() {
		Counter++;
		System.out.println(Counter);
	}

	/**
	 * Reset Counter if cat intersects with BadFish
	 */
	public void reset() {
		Counter = 0;
	}

	public static void main(String[] args) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame_Cat());
			container.setDisplayMode(1920, 1080, false);
			//1920,1080
			container.setTargetFrameRate(60);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
