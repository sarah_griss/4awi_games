package at.sarah.griss.games.basics.oo;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tests.AnimationTest;

import at.sarah.griss.games.basics.oo.actors.Actors;
import at.sarah.griss.games.basics.oo.actors.Bullet;
import at.sarah.griss.games.basics.oo.actors.HTLCircle;
import at.sarah.griss.games.basics.oo.actors.HTLOval;
import at.sarah.griss.games.basics.oo.actors.HTLRectangle;
import at.sarah.griss.games.basics.oo.actors.ShootingStar;
import at.sarah.griss.games.basics.oo.actors.Snowflake;
import at.sarah.griss.games.basics.oo.actors.StarShip;
import at.sarah.griss.games.basics.oo.movestrategy.MoveLeftRightStrategy;
import at.sarah.griss.games.basics.oo.movestrategy.MoveStrategy;

public class FirstGame extends BasicGame {
	private List<Actors> actors;
	private StarShip starship;
	private Image background;
	
	

	public FirstGame() {
		super("Firts Game");
	}

	@Override
	public void render(GameContainer gameContainer, Graphics graphics) throws SlickException {

		for (Actors actor : actors) {
			actor.render(graphics);
		}
	}

	@Override
	public void init(GameContainer gameContainer) throws SlickException {
		this.actors = new ArrayList<>();

//		for (int i = 0; i < 100; i++) {
//			this.actors.add(new Snowflake(Snowflake.size.small, 0.3));
//		}
//
//		for (int i = 0; i < 100; i++) {
//			this.actors.add(new Snowflake(Snowflake.size.medium, 0.4));
//		}
//
//		for (int i = 0; i < 100; i++) {
//			this.actors.add(new Snowflake(Snowflake.size.large, 0.5));
//		}

		MoveStrategy mls = new MoveLeftRightStrategy(20, 20);
		this.starship = new StarShip(gameContainer);
//		this.actors.add(new HTLRectangle(10, 50, 40, 40));
//		this.actors.add(new HTLOval(350, 10, 50, 30));
//		this.actors.add(new HTLCircle(100, 100, mls));
		// this.actors.add(new ShootingStar(0.9, 5));
		this.actors.add(starship);
	}

	@Override
	public void update(GameContainer gameContainer, int delta) throws SlickException {
		for (Actors actor : actors) {
			actor.update(delta);
		}

	}

	@Override
	public void keyPressed(int key, char c) {
		// TODO Auto-generated method stub
		super.keyPressed(key, c);

		if (key == Input.KEY_SPACE) {
			this.actors.add(new Bullet(StarShip.getX(), StarShip.getY()));
		}

	}

	@Override
	public void keyReleased(int key, char c) {
		// TODO Auto-generated method stub
		super.keyReleased(key, c);

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new FirstGame());
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
}
