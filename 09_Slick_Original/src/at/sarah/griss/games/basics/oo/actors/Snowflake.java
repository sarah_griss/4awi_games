package at.sarah.griss.games.basics.oo.actors;

import java.util.ArrayList;
import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

public class Snowflake implements Actors {
	private int size;
	private double speed;
	private double x, y;

	public enum size {
		small, medium, large
	}

	public Snowflake(size size, double speed) {

		this.speed = speed;
		setRandomPosition();

		if (size == size.small) {
			this.size = 5;
		} else if (size == size.medium) {
			this.size = 8;
		} else {
			this.size = 10;
		}
	}

	private void setRandomPosition() {
		Random r = new Random();
		this.y = r.nextInt(600) - 600;
		this.x = r.nextInt(800);
	}

	public void update(int delta) {

		this.y += (double) delta * this.speed;

		if (this.y > 800) {
			setRandomPosition();
		}
	}

	public void render(Graphics graphics) {

		graphics.setColor(Color.white);

		graphics.drawOval((int) this.x, (int) this.y, this.size, this.size);
		graphics.fillOval((int) this.x, (int) this.y, this.size, this.size);

	}

}
