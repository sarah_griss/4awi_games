package at.sarah.griss.games.basics.oo.catgame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class BadFish implements Enemy {

	private int size = 8;
	private double speed = 0.7;
	private float x, y;
	private Image BadFish;
	private Shape shape;
	private boolean collided = false;

	public BadFish(GameContainer container) {
		setRandomPosition();
		this.shape = new Rectangle(x, y, 23, 60);

		try {
			this.BadFish = new Image("testdata/bfish2.png");
		} catch (SlickException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public double getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	private void setRandomPosition() {
		Random r = new Random();
		this.y = r.nextInt(1080) - 1080;
		this.x = r.nextInt(1800);
	}

	public void update(int delta) {

		this.y += (double) delta * this.speed;

		if (this.y > 1080) {
			setRandomPosition();
			this.collided = false;
		}

		this.shape.setY(this.y);
		this.shape.setX(this.x);

	}

	public void render(Graphics graphics) {

		if (collided == true) {

		} else {
			this.BadFish.draw((int) this.x, (int) this.y);
			graphics.setColor(Color.transparent);
			graphics.draw(this.shape);
		}

	}

	public boolean getCollision() {
		return collided;
	}

	public void setCollission(boolean status) {
		collided = status;
	}

	public Shape getShape() {
		return this.shape;
	}

	public Object getCollisionObject() {
		return this;
	}

}
